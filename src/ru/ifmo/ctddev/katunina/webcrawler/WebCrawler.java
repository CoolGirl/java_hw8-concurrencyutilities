package ru.ifmo.ctddev.katunina.webcrawler;

import info.kgeorgiy.java.advanced.crawler.CachingDownloader;
import info.kgeorgiy.java.advanced.crawler.Crawler;
import info.kgeorgiy.java.advanced.crawler.Document;
import info.kgeorgiy.java.advanced.crawler.Downloader;


import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class WebCrawler implements Crawler {
    public static final int AVAILABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();
    AtomicInteger downloadCount = new AtomicInteger(0);
    final Downloader downloader;
    int downloaders;
    int extractors;
    ThreadPoolExecutor downloadersPool;
    ThreadPoolExecutor extractorsPool;
    ConcurrentHashMap<String,String> links;

    public static void main(String[] args) throws IOException {
        WebCrawler webCrawler = new WebCrawler(new CachingDownloader(),
                args.length > 1 ? Integer.parseInt(args[1]) : AVAILABLE_PROCESSORS,
                args.length > 2 ? Integer.parseInt(args[2]) : AVAILABLE_PROCESSORS,
                args.length > 3 ? Integer.parseInt(args[3]) : 0);
        webCrawler.download(args[0], 3);
        webCrawler.close();
    }

    public WebCrawler(Downloader downloader, int downloaders, int extractors, int perHost) {
        this.downloader = downloader;
        System.out.print(downloader.getClass());
        this.downloaders = downloaders;
        this.extractors = extractors;
        System.out.println("Crawler ctor: "+downloaders+" dlrs, "+extractors+" extrs");
        downloadersPool = new ThreadPoolExecutor(1,
                downloaders, 1000, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());
        extractorsPool = new ThreadPoolExecutor(1,
                extractors, 1000, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());
    }

    @Override
    public List<String> download(String url, int depth) throws IOException {
        CountUpDownLatch latch = new CountUpDownLatch(0);
        downloadersPool.submit(new DownloadingTask(depth, url, latch));
        System.out.println("after submit");
        try {
            latch.await();
        } catch (InterruptedException e) {
            System.out.println("Thread was interrupted.");
        }
        close();
        return new ArrayList<>(links.keySet());
    }

    @Override
    public void close() {
        extractorsPool.shutdown();
        downloadersPool.shutdown();

    }

    private abstract class Task implements Runnable {
        protected CountUpDownLatch latch;

        public Task(CountUpDownLatch latch) {
            this.latch = latch;
            latch.countUp();
        }
    }

    private class DownloadingTask extends Task {

        String link;
        int depth;

        public DownloadingTask(int depth, String link, CountUpDownLatch latch) {
            super(latch);
            this.depth = depth;
            this.link = link;
        }

        @Override
        public void run(){
            try {
                System.out.println("before submit download");
                try {
                    if (downloaders == Integer.MAX_VALUE){
                        downloadCount.getAndIncrement();
                    }
                    System.out.println("before submit " + downloadCount);
                    System.out.println(link);
                    Document document = downloader.download(link);
                    System.out.println("Document is ready");
                    ExtractionTask extractionTask = new ExtractionTask(document,depth,latch);
                    System.out.println("We've constructed extraction task");
                    extractorsPool.submit(extractionTask);
                    System.out.println("after submit");
                    System.out.println("Submitted extraction task");
                    latch.countDown();
                } catch (IOException e) {
                    System.out.println("I/O error has occurred.");
                }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
        }
    }

    private class ExtractionTask extends Task {
        Document document;
        int depth;

        public ExtractionTask(Document document, int depth, CountUpDownLatch latch) {
            super(latch);
            this.document = document;
            this.depth = depth;
        }

        @Override
        public void run() {
            try {
                List<String> currentLinks = document.extractLinks();
                System.out.println("before all submit");
                currentLinks.stream().filter(s -> links.putIfAbsent(s,s)==null && depth > 1).
                        forEach(s -> {
                            System.out.println("Before submit download " + s);
                            downloadersPool.submit(new DownloadingTask(depth - 1, s, latch));
                            System.out.println("After submit download " + s);
                        });
                System.out.println("after all submit");
                latch.countDown();
            } catch (IOException e) {
                System.out.println("I/O error has occurred.");
            }
        }
    }

}
